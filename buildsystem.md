# ARPA2 CMake Usage

>   *Short description of ARPA2 CMake best-practices.*

ARPA2 (sub-)projects and products are use [CMake][CMake] (3.13 or later)
for configuration and
building; CMake is a meta-build system, that generates files for
an actual build system that does the work (e.g. CMake generates Makefiles
or ninja configuration, and you run make or ninja to do the build itself).

This document describes the way we use CMake and CMake modules.

## Files

An ARPA2 repository will contain a `CMakeLists.txt` at the top-level
that drives the build for that repository. **Some** also contain a
`Makefile` that serves as a convenience for making an out-of-source
build directory, running cmake, and doing the build.

> 2021-09: The "convenience" `Makefile` is deprecated

## CMake Library

There is a central library of CMake modules in the [*arpa2cm* repository][arpa2cm].
That is where to find the canonical versions of CMake modules that are used
by ARPA2 (sub-)projects. Each ARPA2 (sub-)project should find and use
the *arpa2cm* modules and CMake-helpers to achieve a consistent configuration
and packaging setup.

Documentation on how to use *arpa2cm* is in that repository.

Some ARPA2 (sub-)projects have a `cmake/` directory in the top-level. This
is where the **custom** CMake modules that the (sub-)project uses, live.
This should be the case only in exceptional circumstances, and
modules will tend to migrate towards *arpa2cm* when they are used in multiple
locations.


## CMake Coding Style

In general, we follow the coding style already established in any given
CMake file. Copy the existing style or crib a file from another ARPA2
(sub-)project. At some point we will switch to a CMake-formatter and
drop the idea of a specific ARPA2-style.

- 4 space indents; the standard `.editorconfig` for ARPA2 projects
  sets that up.
- use mixed-case names for packages and project names, e.g. "LillyDAP",
  instead of making them all-caps.
- for project-level variables, use all-caps.
- for (macro-)internal variables, use an underscore and all-lower, e.g. `_foo`


## CMake Sample

See the [Quick-DER repository][Quick-DER] for a sample `CMakeLists.txt`, since that is
a fairly straightfoward one, and extensively documented.


[CMake]: https://cmake.org/cmake/help/v3.13/
[arpa2cm]: https://gitlab.com/arpa2/arpa2cm/
[Quick-DER]: https://gitlab.com/arpa2/quick-der/
