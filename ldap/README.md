# LDAP Data

> This is the canonical, centralized, place for LDAP schemas and
> example data for the ARPA2 project.

This directory contains three main things:
- schemas for the LDAP data structures used by the ARPA2 project
- example data that follows those schemas
- tools to conveniently create LDAP servers that use the schemas and data

There are some extra directories, but each contains a short explanatory README

## Schemas

The `schema/` directory contains the schemas for ARPA2.
Each file is called `arpa2-`*category*`.schema`, where *category*
refers to some category of data in ARPA2. There are no specific
semantics to the categories. Some of the schemas are inter-dependent;
this is documented in the schemas themselves, but in **general**
the idea is to just use all of them.

Configuring an LDAP server is outside the scope of this README,
but see the section on [tools](#Tools) for information on how
to set up an ARPA2-specific LDAP server in a container.

> **NOTE** that adding a schema to the `schema/` directory doesn't
> make it accessible to any particular LDAP server. You need to
> configure the LDAP server as well: for the **example** LDAP server
> you should add the schema to `tools/configuration/slapd.conf.in`.

## Example Data

The `data/` directory contains the example data for ARPA2.
This is a curated collection of LDAP objects that illustrates
how the schemas can be used.

The example data is relatively static; there are tools (such as the
arpa2shell) that can be used to modify the example data, and
project participants are encouraged to produce and submit more
sample data to show a vibrant datastore for an ARPA2 environment.

The files in this directory are read in **in order**
when populating a new LDAP server. This is because there
are DIT-dependencies (the tree needs to be built up with
a suitable *dn* path back to the root). Files are therefore
named *number*-*category*`.ldif`, where *number* is used to
enforce the order, and *category* roughly corresponds to
the categories of the schemas. The category at least should
say what kind of ARPA2-feature is exhibited by that sample data.

### Modifying Live Example Data

One way to make the example data "live" is to modify it with
standard LDAP tools. The directory `data/updates/` contains
LDIF files that can be used to modify the example data.
This can be useful to test that **other** tooling picks up
the changes successfully.

You can use specialized tools like the arpa2shell to
connect to, and modify, the DIT. That is outside the scope of this
README.

To perform modifications using these LDIF files, you need *ldapsearch*
and *ldapmodify*. Assuming your LDAP server is running on `$LDAP_HOST`
on port `$LDAP_PORT`, with the lousy security settings like
the example configuration uses, then you can run updates like this:

    ldapmodify -h $LDAP_HOST -p $LDAP_PORT \
        -x -w sekreet -D "o=arpa2.net,ou=InternetWide" \
        -f data/updates/ldif-update-persons1


## Tools

The `tools/` directory contains tooling to set up an example LDAP server
locally, and a Dockerfile that builds and configures a Docker image
with such an LDAP server, accessible from outside of the image.

**The setup scripts have no security configured**. The setup script
will clobber any local LDAP configuration and replace all the data with
ARPA2 example data. Use with care.

> For a quick "I want a running example LDAP server" situation,
> follow the Quick Setup section suitable for your operating
> environment. Note that the example LDAP server is terribly
> insecure; do not expose it to any networks.


### Quick Setup (FreeBSD)

 - Create a jail.
 - In the jail, install OpenLDAP.
 - Enable OpenLDAP.
 - Copy the **entire** `ldap/` tree from this repository into the jail.
 - Then run the config script from this repository, twice:
   once for configuring, once to load data.

```
pkg install openldap-server openldap-client
sysrc slapd_enable=yes
cd /path/to/copied/ldap/tools/
sh setup.sh config
sh setup.sh initial
```

### Quick Setup (Debian)

 - Install OpenLDAP and some support tools.
 - Enable OpenLDAP.
 - Copy the **entire** `ldap/` tree from this repository into the target.
 - Then run the config script from this repository, in the target, twice:
   once for configuring, once to load data.

```
SLAPD_PASSWORD=sekreet apt-get install slapd ldap-utils procps
cd /path/to/copied/ldap/tools/
sh setup.sh config
sh setup.sh initial
```

### Quick Setup (Docker)

There is a *Dockerfile* in this directory that uses a Debian
stable image and performs the quick setup steps for Debian
in the container.


### Slow Setup (Server)

The script `ldap/setup.sh` can be used to configure an LDAP server.
It places a completely unsecure OpenLDAP configuration file
in the correct place for FreeBSD or Debian -- run this in a
Docker or in a jail.

```
   sh tools/setup.sh config
```


### Slow Setup (Data)

The script can **also** completely replace the data in an LDAP
server with example data for ARPA2. This is destructive!

```
   sh tools/setup.sh initial
```

The existing database is removed. Sample data is loaded.
