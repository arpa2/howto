#! /bin/sh

# Configures and fills an LDAP server with (example) data for ARPA2
# demo purposes. Also can scrub a slapcat dump for re-use.

### OS-SPECIFIC CONFIGURATION
#
#
case x`uname` in
	xFreeBSD)
		LDAP_ETC=/usr/local/etc/openldap
		LDAP_RUN=/var/run/openldap
		LDAP_DB=/var/db/openldap-data
		stop_ldap() { service slapd stop ; }
		start_ldap() { service slapd start ; }
		;;
	xLinux)
		# Assume Debian
		LDAP_ETC=/etc/ldap
		LDAP_RUN=/var/run
		LDAP_DB=/var/lib/ldap
		stop_ldap() { pkill slapd ; sleep 2 ; }
		start_ldap() { /usr/sbin/slapd & sleep 2 ; }
		;;
	*)
		echo "! Unknown OS" `uname`
		exit 1
		;;
esac

### FIND DATA FILES
#
# This script can be called as ./setup.sh or as tools/setup.sh,
# and the data it needs lives in tools/ and in schema/ and in data/
# so figure out what is where.

my_dir_relative=`dirname "$0"`
my_dir=`cd "$my_dir_relative" && pwd -P`
test -n "$my_dir" || { echo "! Can't find directory for LDAP data" ; exit 1 ; }
test -d "$my_dir" || { echo "! Can't find absolute directory for LDAP data" ; exit 1 ; }

# The script lives in tools/, that's also where the config is
# while data/ and schema/ can be found relative to that.
CONFIG_DIR="$my_dir"
DATA_DIR="$my_dir/../data"
SCHEMA_DIR="$my_dir/../schema"

test -d "$CONFIG_DIR" || { echo "! Can't find tools/ dir for configuration" ; exit 1 ; }
test -d "$DATA_DIR" || { echo "! Can't find data/ dir for initial data" ; exit 1 ; }
test -d "$SCHEMA_DIR" || { echo "! Can't find schema/ dir for schemas" ; exit 1 ; }


### PERFORM ACTIONS
#
#
case x"$1" in
	xconfig)	
		stop_ldap
		mkdir -p $PREFIX$LDAP_ETC $PREFIX$LDAP_ETC/schema/
		cp $CONFIG_DIR/ldap.conf $PREFIX$LDAP_ETC/ldap.conf
		cp $SCHEMA_DIR/* $PREFIX$LDAP_ETC/schema/
		sed \
			-e "s+@@LDAP_ETC@@+$LDAP_ETC+g" \
			-e "s+@@LDAP_RUN@@+$LDAP_RUN+g" \
			-e "s+@@LDAP_DB@@+$LDAP_DB+g" \
			$CONFIG_DIR/slapd.conf.in > $PREFIX$LDAP_ETC/slapd.conf
		start_ldap
		;;
	xinitial)
		stop_ldap
		rm $PREFIX$LDAP_DB/*.mdb
		start_ldap
		for f in $DATA_DIR/[0-9]*.ldif
		do
			ldapadd -f "$f" -x -w sekreet -D "o=arpa2.net,ou=InternetWide"
		done
		;;
	xscrub)
		FILE="$2"
		test -f "$FILE" || { echo "! <scrub> needs a file." ; exit 1 ; }
		python -c '
from sys import stdin
skipping=False
skip_fields=("structuralObjectClass", "entryUUID", "creatorsName", "createTimestamp", "entryCSN", "modifiersName", "modifyTimestamp")
for line in stdin.readlines():
    if skipping and line.startswith(" "):
        continue
    skipping=any([line.startswith(f+":") for f in skip_fields])
    if not skipping:
        print(line.rstrip())
' < "$FILE"
		;;
	*)
		echo "! Unknown command" "$1"
		exit 1
		;;
esac
