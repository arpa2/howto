# ARPA2 Release Process

>   A quick description of how we like to tag our versions and release them.

## Version Tagging

-   In our versioning systems, we **do not** tag or release alpha, beta, or RC
    versions. Only full-on releases are tagged, although it's legitimate to
    release early and release often.
-   The tags are written with semantic tagging in mind: Major, minor, patch
    and the form is `vM.m.p` (e.g. `v0.0.1` or `v3.2.23`).
-   It is OK to have **no** patch version, e.g. `v0.1` is also ok.
-   Version tags are always applied to an entry from the master/default branch
    of a versioning system.
-   Match the tag with the version number in the
    [`"mkhere"`](https://gitlab.com/arpa2/mkhere) file on the `default_VERSION`
    command line to cause it to make your new version **available** to all
    other builds, including as a dependency in GitLab-CI builds that use
    "mkhere" as their ports tree.
-   Match the tag with the version number in the `CMakeLists.txt` file
    in the `project()` command to impose a **requirement** on your new
    version.  Please **do not** update `CMakeLists.txt` to a version beyond
    the version in "mkhere" as that causes automation to break.
-   In the `CMakeLists.txt`, use the ARPA2CM module `MacroGitVersionInfo`
    to consistently get a version header and versioning information in
    the application itself.

## Release Process (with git)

1.  Annotate your code with the version strings, etcetera
    - Update `CMakeLists.txt` with the new version in the `project()` command
    - Update any `setup.py` with the new version in the `version` string, but
      strip the leading `v` in the tag.
    - Add an entry to the `CHANGES` file. Copy the form used there,
      which uses a header with the *vM.m.p* version name and a date in
      ISO format (YYYY-MM-DD).
    - If your project uses Python, edit its `setup.py` file(s) to set the
      new version number in `version = '3.2.23'` strings (drop the `v` to be
      compatible with PyPi).   *Did you test the script about to be released?*
2.  Commit and push your code.
3.  Create a version tag and push it to the publicly visible repository.
    Sign the tag with your GPG key (`-s` flag).  You can do this separately,
    and on a different machine (that is secure if the hash is correct).
    ```
    git tag -s v0.7.3
    git push --tags
    ```
4.  Go to the GitLab page for the project, and select 
    *Repository* :arrow_right: *Tags*, 
    and for the tag you just pushed, click *Edit release notes*. Write something 
    -- perhaps copy the `CHANGES` entry and save it.
5.  Update the `default_VERSION` command in the ["mkhere" script](gitlab.com/arpa2/mkhere)
    for the project and push that to `master` so it is used as the default
    dependency.  This immediately impacts many GitLab-CI pipelines.
6.  Update project metadata in the *howto* repository, like the new
    version number and changed dependencies.  Look for `data/stack.dot.in` and
    run the `Makefile` to generate `stack.png` from it.
7.  Do the **social** part of the release: announce it on the mailing list
    and elsewhere.

