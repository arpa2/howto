# The ARPA2 "HowTo" Files

> *This repository contains procedures and scripts that we like to use for
> all ARPA2 work, so it becomes a bit more consistent in how it works.*

 - [Buildsystem](buildsystem.md) information, on how we use CMake,
   and what a good top-level directory for an ARPA2 project looks like.
 - [Releases](release.md) are done independently across different
   ARPA2 projects. In general newer releases lower down in the stack
   percolate upwards, triggering minor updates elsewhere.
 - [Blogging](blogging.md) is part of the **public relations** for ARPA2.
   Blog articles go up on [InternetWide.org](http://internetwide.org/).
   See the [blogging](blogging.md) notes for an explanation of the format.

> The following are out-of-date and generally useless documents
> describing ways that the ARPA2 project worked in the past.
> This was the explicitly-Docker-centric phase of the project,
> now replaced by `mkhere`.

 - [Packaging](packaging.md) of ARPA2 projects for Docker (our main
   delivery pipeline) and as native packages.
 - [Deliverables](deliverables.md) describes what kind of software
   artifacts come out of an ARPA2 project; in particular, how we
   use the CMake build inside of Docker.
 - [Testing Platform](testplatform.md) for ARPA2 projects.

